If DB_ID('MXH_DULICH') is NULL
	CREATE Database MXH_DULICH
GO
USE MXH_DULICH

CREATE TABLE NHATKIHANHTRINH 
(
	Ma INT NOT NULL IDENTITY,
	MoTa nvarchar(2000),
	ChuyenDi INT,
	
	PRIMARY KEY (Ma),		
)
CREATE TABLE SUKIEN
(
	ThoiGian datetime,
	NhatKi int,
	
	NoiDung nvarchar (3000),
	
	primary key (NhatKi, ThoiGian),		
)
CREATE TABLE CHUYENDI
(
	Ma INT NOT NULL IDENTITY,
	MoTa nvarchar(2000),
	
	Primary key (Ma),	
)

CREATE TABLE DIADIEM
(
	Ma INT NOT NULL IDENTITY,
	Ten nvarchar (100),
	DiaChi nvarchar (1000),
	MoTa nvarchar(2000),
	
	Primary key (Ma),	
)

CREATE TABLE TOADO_DIADIEM --Google map
(
	DiaDiem INT,
	HoanhDo varchar(50), -- Longitude
	TungDo varchar(50), --Latitude
	
	primary key (DiaDiem),
	
)

CREATE TABLE DIADIEM_CHUYENDI
(
	ChuyenDi int,
	DiaDiem int,
	
	Primary key(ChuyenDi, DiaDiem),
	
		
)



ALTER TABLE NHATKIHANHTRINH ADD
	Constraint FK_NKHT_CHUYENDI foreign key (ChuyenDi) References CHUYENDI(Ma)
	
	
ALTER TABLE TOADO_DIADIEM ADD
	Constraint FK_TD_DIADIEM foreign key(DiaDiem) references DIADIEM(Ma)


ALTER TABLE DIADIEM_CHUYENDI ADD	
	Constraint FK_DDCD_DIADIEM foreign key(DiaDiem) references DIADIEM(Ma),
	Constraint FK_DDCD_CHUYENDI foreign key(ChuyenDi) references CHUYENDI(Ma)

ALTER TABLE SUKIEN ADD
	constraint FK_SK_NHATKI foreign key(NhatKi) references NHATKIHANHTRINH(Ma)


--DROP TABLE
/*


*/
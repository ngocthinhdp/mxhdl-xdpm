Use master
Go
If DB_ID('db484') is not NULL
	Drop Database db484
Go
Create Database db484
Go
Use db484

Create Table NGUOIDUNGCANHAN 
(
	TaiKhoan		nvarchar(50) not NULL,
	MatKhau			nvarchar(50) not NULL,
	Ten				nvarchar(50),
	GioiTinh		nvarchar(3),
	Check (GioiTinh in ('Nam', 'Nu')),
	NgaySinh		DateTime,
	DiaChi			nvarchar,
	DienThoai		varchar,	
	
	Constraint PK_NGUOIDUNGCANHAN Primary key (TaiKhoan)
)

Create Table NGUOIDUNGDOANHNGHIEP
(	
	TaiKhoan		nvarchar(50) not NULL,
	MatKhau			nvarchar(50) not NULL,
	Ten				nvarchar(50),
	DiaChi			nvarchar,
	DoTinCay		int,
	MoTa			nvarchar,
	
	Constraint PK_NGUOIDUNGDOANHNGHIEP Primary key (TaiKhoan)	
)

Create Table CANHAN_BANBE
(
	CaNhan	nvarchar(50),
	BanBe	nvarchar(50),
	
	Constraint PK_CANHAN_BANBE Primary key (CaNhan,BanBe)
)

Create Table YEUTHICH_DOANHNGHIEP
(
	CaNhan		nvarchar(50),
	DoanhNghiep	nvarchar(50),
	
	Constraint PK_YEUTHICH_DOANHNGHIEP Primary key (CaNhan,DoanhNghiep)
)

Create Table DANHGIADOANHNGHIEP
(
	CaNhan		nvarchar(50),
	DoanhNghiep	nvarchar(50),
	Diem		tinyint,
	Check (Diem in (0,1,2,3,4,5)),
	ThoiGian	DateTime,
	
	Constraint PK_DANHGIADOANHNGHIEP Primary key (CaNhan,DoanhNghiep)
)


Create Table DOANHNGHIEP_LIENKET
(
	DoanhNghiep			nvarchar(50),
	DoanhNghiepLienKet	nvarchar(50),
	
	Constraint PK_DOANHNGHIEP_LIENKET Primary key (DoanhNghiep, DoanhNghiepLienKet)
)

-- Cac Khoa Ngoai

Alter Table CANHAN_BANBE Add
	Constraint FK_CANHAN Foreign key (CaNhan) References NGUOIDUNGCANHAN(TaiKhoan),
	Constraint FK_BANBE Foreign key (BanBe) References NGUOIDUNGCANHAN(TaiKhoan)
	
Alter Table DOANHNGHIEP_LIENKET Add
	Constraint FK_DOANHNGHIEP Foreign key (DoanhNghiep) References NGUOIDUNGDOANHNGHIEP(TaiKhoan),
	Constraint FK_LIENKET Foreign key (DoanhNghiepLienKet) References NGUOIDUNGDOANHNGHIEP(TaiKhoan)
	
Alter Table DANHGIADOANHNGHIEP Add
	Constraint FK_CANHANDANHGIA Foreign key (CaNhan) References NGUOIDUNGCANHAN(TaiKhoan),
	Constraint FK_DANHGIADOANHNGHIEP Foreign key (DoanhNghiep) References NGUOIDUNGDOANHNGHIEP(TaiKhoan)
	
Alter Table YEUTHICH_DOANHNGHIEP Add
	Constraint FK_CANHANYEUTHICH Foreign key (CaNhan) References NGUOIDUNGCANHAN(TaiKhoan),
	Constraint FK_DOANHNGHIEPDUOCTHICH Foreign key (DoanhNghiep) References NGUOIDUNGDOANHNGHIEP(TaiKhoan)
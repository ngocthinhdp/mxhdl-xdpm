use master
GO
--If DB_ID('1012511') is not NULL
	--DROP DATABASE 1012511
--GO
--CREATE DATABASE 1012511
--GO
If DB_ID('MXH_DULICH') is NULL
	CREATE Database MXH_DULICH
GO
use MXH_DULICH

GO
Create Table HINHANH	
(
	Ma		INT NOT NULL IDENTITY,		
	MoTa		nvarchar(1000), --?
	TieuDe			nvarchar(50),
	DuLieu		nvarchar(250),
	KichThuoc			int, 
	MaAlbum INT
	Constraint PK_HINHANH Primary key (Ma)
)
GO
Create Table VIDEO	
(
	Ma			INT NOT NULL IDENTITY,		
	MoTa			nvarchar(1000),
	TieuDe			nvarchar(50),
	DuLieu		nvarchar(250),
	ThoiLuong		time,
	KichThuoc		int,
	Constraint PK_VIDEO Primary key (Ma)
)
GO
Create Table DAPHUONGTIEN		
(
	Ma INT,		
	LoaiDaPhuongTien	nchar(10),
	
	Check (LoaiDaPhuongTien in ('HINHANH', 'VIDEO')),
		
	Constraint PK_DAPHUONGTIEN Primary key (Ma, LoaiDaPhuongTien)
)
GO
Create Table ALBUM			
(
	Ma		INT NOT NULL IDENTITY,		
	TieuDe			nvarchar(30),
	MoTa			nvarchar(1000),
	Constraint PK_ALBUM Primary key (MA)
)
GO
Create Table QUANGCAO			
(
	Ma			INT NOT NULL IDENTITY,		
	TieuDe			nvarchar(30),
	NoiDung			nvarchar(3000),
	DoTinCay		int, --?
	Constraint PK_QUANGCAO Primary key (Ma)
)
GO
Create Table BAIVIET			
(
	Ma		INT NOT NULL IDENTITY,		
	TieuDe			nvarchar(30),
	NoiDung			nvarchar(3000), --Xem x�t t�ng l�n
	Constraint PK_BAIVIET Primary key (MA)
)
GO
	
-- Cac Khoa Ngoai
Alter Table HINHANH Add
	Constraint FK_HINHANH_ALBUM Foreign Key(Ma) References ALBUM(Ma)

--DROP TABLE
/*
DROP TABLE ALBUM
DROP TABLE DAPHUONGTIEN
DROP TABLE HINHANH
DROP TABLE VIDEO
DROP TABLE QUANGCAO
DROP TABLE BAIVIET


*/	
﻿--INSERT DATABASE
USE MXH_DULICH



--ALBUM
INSERT INTO ALBUM(MoTa, TieuDe) VALUES(N'Album này chứa những hình ảnh tạm thời của Nguyễn Cường Thịnh', N'Album tạm')
INSERT INTO ALBUM(MoTa, TieuDe) VALUES(N'Lần đầu tiên đi phượt Tây Ninh, mò đường đi mà cứ sợ bị lạc! Chạy miệt mài tới được Gò Dầu thì thấy bảng ghi "Thị xã Tây Ninh 36KM", đọc xong muốn xỉu!', N'Tây Ninh - Một lần trong đời')
INSERT INTO ALBUM(MoTa, TieuDe) VALUES(N'Album này chứa những hình ảnh tạm thời của Nguyễn Ngọc Thịnh', N'Album tạm')
INSERT INTO ALBUM(MoTa, TieuDe) VALUES(N'Album này chứa những hình ảnh tạm thời của Phạm Văn Tiến', N'Album tạm')


--HINH ANH
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Ảnh địa diện', N'', 120, 'UserDatas/Images/1.jpg', 1)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Ảnh bìa', N'', 120, 'UserDatas/Images/2.jpg', 1)

INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/3.jpg', 2)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/4.jpg', 2)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/5.jpg', 2)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/6.jpg', 2)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/7.jpg', 2)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/8.jpg', 2)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Tây Ninh', N'', 120, 'UserDatas/Images/9.jpg', 2)

INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Ảnh địa diện', N'', 120, 'UserDatas/Images/10.jpg', 3)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Ảnh bìa', N'', 120, 'UserDatas/Images/11.jpg', 3)

INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Ảnh địa diện', N'', 120, 'UserDatas/Images/12.jpg', 4)
INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum) VALUES(N'Ảnh bìa', N'', 120, 'UserDatas/Images/13.jpg', 4)

--NGUOI DUNG CA NHAN
INSERT INTO CANHAN(TaiKhoan, MatKhau) VALUES('ncthinh@gmail.com', '01091992')
INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES('ncthinh@gmail.com', N'Nguyễn Cường Thịnh', 'NAM', '01/09/1992', N'Thành phố Hồ Chí Minh', '01676019511', N'Sinh viên', 1, 2, N'For a root, for a leaf, for a branch, for a tree, for something, somebody that reminded them of me')

INSERT INTO CANHAN(TaiKhoan, MatKhau) VALUES('nnthinh@gmail.com', '01091992')
INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES('nnthinh@gmail.com', N'Nguyễn Ngọc Thịnh', 'NAM', '01/09/1992', N'Đức Phổ - Quảng Ngãi', '01676019511', N'Sinh viên', 10, 11, N'Một ngày tuyệt vời ;v!!!')

INSERT INTO CANHAN(TaiKhoan, MatKhau) VALUES('pvtien@gmail.com', '01091992')
INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES('pvtien@gmail.com', N'Phạm Văn Tiên', 'NAM', '01/09/1992', N'Đức Phổ - Quảng Ngãi', '01676019511', N'Sinh viên', 12, 13, N'Chuẩn cao ;v!!!')

--THICH
INSERT INTO THICH_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN) VALUES('nnthinh@gmail.com', 4, 'CANHAN', 'ALBUM','5/2/2013 22:55:02')
INSERT INTO THICH_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN) VALUES('pvtien@gmail.com', 4, 'CANHAN', 'ALBUM', '5/2/2013 22:55:02')
--BINH LUAN
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('nnthinh@gmail.com', 2, 'CANHAN', 'ALBUM','5/2/2013 23:55:02', N'SPAM :">')
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('nnthinh@gmail.com', 2, 'CANHAN', 'ALBUM','5/2/2013 23:52:02', N'SPAM:">')
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('nnthinh@gmail.com', 2, 'CANHAN', 'ALBUM','5/2/2013 23:53:02', N'Toàn là en ún ko z pa :p, cơ mà thấy hình cái ham :">')
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('pvtien@gmail.com', 2, 'CANHAN', 'ALBUM','5/2/2013 22:59:02', N'Đã vậy! Bữa nào làm kèo nữa nhớ hú nhoé :v')
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('nnthinh@gmail.com', 2, 'CANHAN', 'ALBUM','5/3/2013 7:55:02', N'Thiệt là bịnh quá mà!')
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('nnthinh@gmail.com', 4, 'CANHAN', 'ALBUM','5/2/2013 23:53:02', N'Toàn là en ún ko z pa :p, cơ mà thấy hình cái ham :">')
INSERT INTO CAMNHAN_NGUOIDUNG(NGUOIDUNG, TAINGUYEN, LOAIND, LOAITN, THOIGIAN, CAMNHAN) VALUES('pvtien@gmail.com', 4, 'CANHAN', 'ALBUM','5/2/2013 22:59:02', N'Đã vậy! Bữa nào làm kèo nữa nhớ hú nhoé :v')

--BAI VIET
INSERT INTO BAIVIET(TieuDe, NoiDung) VALUES(N'Con chuột tinh khôn', N'Có con mèo ngồi rình những con cá béo mập trong chậu nước trước nhà mà không nghĩ ra cách bắt cá. Bỗng có con chuột mò đến trên thành chậu, ngó vào đáy nước. Quên phắt đi những con cá, Mèo chộp ngay lấy Chuột, định ăn tươi. Chuột quính quá kinh hãi kêu lên:

- Đừng vội giết tôi. Xin câu lên biếu bác con cá béo ngậy thơm ngon hơn thịt chuột nhiều. 

Nghe nói đến cá, Mèo chịu buông tha, nhưng vẫn coi chừng Chuột cẩn thận. Chuột khẽ đến bên miệng chậu thò đuôi vào loáy ngoáy trong nước một hồi. Cá ngỡ là trùn, bèn cắn ngay đuôi Chuột, liền bị Chuột quẫy đuôi quăng lên bờ. Vừa lúc đó có tiếng người la:

- Chuột ăn trộm cá. 

Mèo bèn chộp ngay Chuột, cắn vào cổ. Còn chủ nhà vừa bắt cá thả vào chậu, vừa lớn tiếng khen:

- Mèo nhà gỏi thật, không có nó thì Chuột đã ăn mất cả rồi!')

INSERT INTO BAIVIET(TieuDe, NoiDung) VALUES(N'Lo trước chắc ăn', N'Vào những ngày hè, Kiến vừa đi dạo hết cánh đồng vừa thu nhặt các hạt lúa mì, lúa mạch để dự trữ lương thực cho mùa đông. Bọ Rầy thấy thế liền chế giễu Kiến phải làm chi cho cực trong lúc các loài vật khác được nghỉ ngơi vui chơi, đắm say vào các cuộc hội hè. Kiến vẫn cứ lặng thinh làm việc. Khi mùa đông đến, trời mưa dầm dề, Bọ Rầy không tìm được thức ăn, đói lả, bèn đến hỏi Kiến vay lương thực. Kiến bảo: "Chị Bọ Rầy ạ, giá trước đây chị cứ lo làm, đừng quở trách gì tôi thì bây giờ đâu đến nỗi chị phải chịu ngồi đói meo!')



--TAI NGUYEN - CA NHAN
INSERT INTO TAINGUYEN_CANHAN(TAIKHOAN, LOAITN, MATN, THOIGIAN) VALUES('ncthinh@gmail.com', 'ALBUM', 2, '5/2/2013 22:00:02')
INSERT INTO TAINGUYEN_CANHAN(TAIKHOAN, LOAITN, MATN, THOIGIAN) VALUES('ncthinh@gmail.com', 'BAIVIET', 1, '5/3/2013 01:00:02')
INSERT INTO TAINGUYEN_CANHAN(TAIKHOAN, LOAITN, MATN, THOIGIAN) VALUES('nnthinh@gmail.com', 'BAIVIET', 2, '5/2/2013 01:00:02')


--NGUOI DUNG
INSERT INTO CANHAN(TaiKhoan, MatKhau) VALUES('test01@gmail.com', '01091992')
INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES('test01@gmail.com', N'Họ tên', 'NAM', '01/09/1992', N'Địa chỉ', 'Số điện thoại', N'Nghề nghiệp', 1, 2, N'Tâm trạng của tôi')

INSERT INTO CANHAN(Taikhoan, Matkhau) VALUES(nphu@gmail.com, 01091992) INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES(N'nphu@gmail.com', N'Nguyễn Phú', 'NAM', '01/09/1992', N'Địa chỉ', 'Số điện thoại', N'Nghề nghiệp', 1, 2, N'Tâm trạng của tôi')
SELECT * FROM CANHAN
SELECT * FROM THONGTINCANHAN
SELECT * FROM HINHANH
UPDATE HINHANH
SET	DuLieu = ''
WHERE Ma = (SELECT THONGTINCANHAN.AnhBia FROM THONGTINCANHAN WHERE THONGTINCANHAN.TaiKhoan = '')

UPDATE THONGTINCANHAN
SET	 AnhBia = (SELECT MAX(MA) FROM HINHANH)
WHERE THONGTINCANHAN.TaiKhoan = 'ncthinh@gmail.com'



SELECT * FROM BAIVIET

SELECT *
FROM CAMNHAN_NGUOIDUNG
WHERE CAMNHAN_NGUOIDUNG.TAINGUYEN = 4 AND CAMNHAN_NGUOIDUNG.LOAITN = 'ALBUM'
ORDER BY (CAMNHAN_NGUOIDUNG.THOIGIAN) ASC

SELECT TOP 10 *
FROM TAINGUYEN_CANHAN 
WHERE TAINGUYEN_CANHAN.TAIKHOAN = 'ncthinh@gmail.com' AND
 (TAINGUYEN_CANHAN.LOAITN = 'BAIVIET' OR TAINGUYEN_CANHAN.LOAITN = 'ALBUM') AND
 TAINGUYEN_CANHAN.THOIGIAN < CONVERT(datetime, '5/3/2013')
ORDER BY(TAINGUYEN_CANHAN.THOIGIAN) DESC

SELECT TOP 10 * FROM TAINGUYEN_CANHAN WHERE TAINGUYEN_CANHAN.TAIKHOAN = 'ncthinh@gmail.com' AND (TAINGUYEN_CANHAN.LOAITN = 'BAIVIET' OR TAINGUYEN_CANHAN.LOAITN = 'ALBUM') AND TAINGUYEN_CANHAN.THOIGIAN < CONVERT(datetime, '5/3/2013 1:02:31 AM') ORDER BY(TAINGUYEN_CANHAN.THOIGIAN) DESC

SELECT *
FROM ALBUM
WHERE ALBUM.Ma = 1;

SELECT * FROM HINHANH WHERE HINHANH.MaAlbum = 2
 
SELECT * FROM TAINGUYEN_CANHAN
SELECT * FROM NGUOIDUNG


SELECT * 
FROM HINHANH
WHERE HINHANH.MaAlbum = 2

UPDATE ALBUM
SET MOTA= N'Lần đầu tiên đi phượt Tây Ninh, mò đường đi mà cứ sợ bị lạc! Chạy miệt mài tới được Gò Dầu thì thấy bảng ghi "Thị xã Tây Ninh 36KM", đọc xong muốn xỉu!',
TIEUDE = N'Tây Ninh - Một lần trong đời'
WHERE MA=2
INSERT INTO TAINGUYEN_CANHAN(TAIKHOAN, MA, LOAITN, THOIGIAN) VALUES('ncthinh@gmail.com', '1', 'ALBUM', '10/02/2012 10:29:01')


SELECT * FROM TAINGUYEN_CANHAN
SELECT * FROM ALBUM
SELECT TOP 5  * FROM HINHANH

SELECT NGUOIDUNG.LOAIND
FROM NGUOIDUNG WHERE NGUOIDUNG.TAIKHOAN = 'ncthinh@gmail.com'

SELECT THONGTINCANHAN.Ten, THONGTINCANHAN.GioiTinh, THONGTINCANHAN.NgaySinh, THONGTINCANHAN.DiaChi, THONGTINCANHAN.NgheNghiep, THONGTINCANHAN.TamTrang, THONGTINCANHAN.DienThoai, ANHDAIDIEN.DuLieu AS AnhDaiDien, ANHBIA.DuLieu AS AnhBia
FROM CANHAN INNER JOIN THONGTINCANHAN ON CANHAN.TaiKhoan = THONGTINCANHAN.TaiKhoan INNER JOIN HINHANH ANHBIA ON THONGTINCANHAN.AnhBia = ANHBIA.Ma INNER JOIN HINHANH ANHDAIDIEN ON THONGTINCANHAN.AnhDaiDien=ANHDAIDIEN.Ma
WHERE CANHAN.TaiKhoan = 'ncthinh@gmail.com' AND CANHAN.MatKhau = '01091992'

SELECT THONGTINCANHAN.Ten, THONGTINCANHAN.GioiTinh, THONGTINCANHAN.NgaySinh, THONGTINCANHAN.DiaChi, THONGTINCANHAN.NgheNghiep, THONGTINCANHAN.TamTrang, THONGTINCANHAN.DienThoai, ANHDAIDIEN.DuLieu AS AnhDaiDien, ANHBIA.DuLieu AS AnhBia FROM CANHAN INNER JOIN THONGTINCANHAN ON CANHAN.TaiKhoan = THONGTINCANHAN.TaiKhoan INNER JOIN HINHANH ANHBIA ON THONGTINCANHAN.AnhBia = ANHBIA.Ma INNER JOIN HINHANH ANHDAIDIEN ON THONGTINCANHAN.AnhDaiDien=ANHDAIDIEN.Ma  WHERE CANHAN.TaiKhoan LIKE 'ncthinh@gmail.com' AND CANHAN.MatKhau LIKE '01091992'

UPDATE HINHANH SET DuLieu = 'UserDatas\Images\14.jpg' WHERE Ma = (SELECT THONGTINCANHAN.AnhBia FROM THONGTINCANHAN WHERE THONGTINCANHAN.TaiKhoan = 'ncthinh@gmail.com')
UPDATE HINHANH SET DuLieu = 'UserDatas\Images\14.jpg' WHERE Ma = (SELECT THONGTINCANHAN.AnhDaiDien FROM THONGTINCANHAN WHERE THONGTINCANHAN.TaiKhoan = 'ncthinh@gmail.com')


SELECT NGUOIDUNG.LOAIND FROM NGUOIDUNG WHERE NGUOIDUNG.TAIKHOAN = 'ncthinh@gmail.com'
SELECT * FROM HINHANH

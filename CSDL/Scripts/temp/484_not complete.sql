--Use master
--Go
--If DB_ID('db484') is not NULL
	--Drop Database db484
--Go
--Create Database db484
--Go
--Use db484

If DB_ID('MXH_DULICH') is NULL
	CREATE Database MXH_DULICH
	
	
Go	
use MXH_DULICH

--CA NHA
Create Table CANHAN 
(
	TaiKhoan		varchar(50) not NULL,
	MatKhau			varchar(50) not NULL,
	
	
	Constraint PK_CANHAN Primary key (TaiKhoan)
)

CREATE TABLE THONGTINCANHAN
(
	TaiKhoan		varchar(50),
	Ten				nvarchar(50),
	GioiTinh		nvarchar(3),
	Check (GioiTinh in ('Nam', 'Nu', 'Khac')),
	NgaySinh		DateTime,
	DiaChi			nvarchar(100),
	NgheNghiep		nvarchar(50),
	TamTrang		nvarchar(500),
	DienThoai		varchar(20),	
	AnhDaiDien		nvarchar(200),
	AnhBia		nvarchar(200),	
	Primary Key(TaiKhoan)
	
	
)

--DOANH NGHIEP
Create Table DOANHNGHIEP
(	
	TaiKhoan		varchar(50) not NULL,
	MatKhau			varchar(50) not NULL,	
	Primary Key(TaiKhoan)
)

CREATE TABLE THONGTINDOANHNGHIEP
(
	TaiKhoan		varchar(50),
	Ten				nvarchar(50),
	DiaChi			nvarchar(100),	
	TamTrang		nvarchar(500),
	DienThoai		varchar(20),	
	AnhDaiDien		nvarchar(200),
	AnhBia		nvarchar(200),	
	MoTa nvarchar(1000),
	
	Primary Key(TaiKhoan)	
)

--BAN BE
Create Table BANBE
(
	CaNhan	varchar(50),
	BanBe	varchar(50),
	
	Constraint PK_CANHAN_BANBE Primary key (CaNhan,BanBe)
)

Create Table YEUTHICH_DOANHNGHIEP
(
	CaNhan		varchar(50),
	DoanhNghiep	varchar(50),
	
	Constraint PK_YEUTHICH_DOANHNGHIEP Primary key (CaNhan,DoanhNghiep)
)

Create Table DANHGIA_DOANHNGHIEP
(
	CaNhan		varchar(50),
	DoanhNghiep	varchar(50),
	Diem		tinyint,
	Check (Diem in (0,1,2,3,4,5)),
	ThoiGian	DateTime,
	
	Constraint PK_DANHGIADOANHNGHIEP Primary key (CaNhan,DoanhNghiep)
)


Create Table DOANHNGHIEP_LIENKET
(
	DoanhNghiep			varchar(50),
	DoanhNghiepLienKet	varchar(50),
	
	Constraint PK_DOANHNGHIEP_LIENKET Primary key (DoanhNghiep, DoanhNghiepLienKet)
)

-- Cac Khoa Ngoai
Alter Table THONGTINCANHAN Add
	Constraint FK_TTCN_CANHAN Foreign Key(TaiKhoan) References CANHAN(TaiKhoan)
	
Alter Table THONGTINDOANHNGHIEP Add
	Constraint FK_TTDN_DOANHNGHIEP Foreign Key(TaiKhoan) References DOANHNGHIEP(TaiKhoan)
	
Alter Table BANBE Add
	Constraint FK_CANHAN Foreign key (CaNhan) References CANHAN(TaiKhoan),
	Constraint FK_BANBE Foreign key (BanBe) References CANHAN(TaiKhoan)
	
Alter Table DOANHNGHIEP_LIENKET Add
	Constraint FK_DOANHNGHIEP Foreign key (DoanhNghiep) References DOANHNGHIEP(TaiKhoan),
	Constraint FK_LIENKET Foreign key (DoanhNghiepLienKet) References DOANHNGHIEP(TaiKhoan)
	
Alter Table DANHGIA_DOANHNGHIEP Add
	Constraint FK_CANHANDANHGIA Foreign key (CaNhan) References CANHAN(TaiKhoan),
	Constraint FK_DANHGIADOANHNGHIEP Foreign key (DoanhNghiep) References DOANHNGHIEP(TaiKhoan)
	
Alter Table YEUTHICH_DOANHNGHIEP Add
	Constraint FK_CANHANYEUTHICH Foreign key (CaNhan) References CANHAN(TaiKhoan),
	Constraint FK_DOANHNGHIEPDUOCTHICH Foreign key (DoanhNghiep) References DOANHNGHIEP(TaiKhoan)
	
	
/*	
DROP TABLE CANHAN
DROP TABLE DOANHNGHIEP
DROP TABLE THONGTINCANHAN
DROP TABLE THONGTINDOANHNGHIEP
DROP TABLE BANBE
DROP TABLE YEUTHICH_DOANHNGHIEP
DROP TABLE DANHGIA_DOANHNGHIEP
DROP TABLE DOANHNGHIEP_LIENKET

--DROP DATABASE MXH_DULICH
*/


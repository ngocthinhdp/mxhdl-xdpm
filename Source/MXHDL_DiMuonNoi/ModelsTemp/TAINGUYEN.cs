using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class TAINGUYEN
    {
        public TAINGUYEN()
        {
            this.CAMNHAN_NGUOIDUNG = new List<CAMNHAN_NGUOIDUNG>();
            this.CHIASE_NGUOIDUNG = new List<CHIASE_NGUOIDUNG>();
            this.THICH_NGUOIDUNG = new List<THICH_NGUOIDUNG>();
            this.TAINGUYEN_CANHAN = new List<TAINGUYEN_CANHAN>();
            this.TAINGUYEN_DOANHNGHIEP = new List<TAINGUYEN_DOANHNGHIEP>();
        }

        public int MA { get; set; }
        public string LOAITN { get; set; }
        public virtual ICollection<CAMNHAN_NGUOIDUNG> CAMNHAN_NGUOIDUNG { get; set; }
        public virtual ICollection<CHIASE_NGUOIDUNG> CHIASE_NGUOIDUNG { get; set; }
        public virtual ICollection<THICH_NGUOIDUNG> THICH_NGUOIDUNG { get; set; }
        public virtual ICollection<TAINGUYEN_CANHAN> TAINGUYEN_CANHAN { get; set; }
        public virtual ICollection<TAINGUYEN_DOANHNGHIEP> TAINGUYEN_DOANHNGHIEP { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class ALBUM
    {
        public ALBUM()
        {
            this.HINHANHs = new List<HINHANH>();
        }

        public int Ma { get; set; }
        public string TieuDe { get; set; }
        public string MoTa { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
        public virtual ICollection<HINHANH> HINHANHs { get; set; }
    }
}

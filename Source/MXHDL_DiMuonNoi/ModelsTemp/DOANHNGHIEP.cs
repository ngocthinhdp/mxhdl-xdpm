using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class DOANHNGHIEP
    {
        public DOANHNGHIEP()
        {
            this.DANHGIA_DOANHNGHIEP = new List<DANHGIA_DOANHNGHIEP>();
            this.TAINGUYEN_DOANHNGHIEP = new List<TAINGUYEN_DOANHNGHIEP>();
            this.DOANHNGHIEP1 = new List<DOANHNGHIEP>();
            this.DOANHNGHIEPs = new List<DOANHNGHIEP>();
            this.CANHANs = new List<CANHAN>();
        }

        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public virtual ICollection<DANHGIA_DOANHNGHIEP> DANHGIA_DOANHNGHIEP { get; set; }
        public virtual ICollection<TAINGUYEN_DOANHNGHIEP> TAINGUYEN_DOANHNGHIEP { get; set; }
        public virtual THONGTINDOANHNGHIEP THONGTINDOANHNGHIEP { get; set; }
        public virtual ICollection<DOANHNGHIEP> DOANHNGHIEP1 { get; set; }
        public virtual ICollection<DOANHNGHIEP> DOANHNGHIEPs { get; set; }
        public virtual ICollection<CANHAN> CANHANs { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class NHATKIHANHTRINH
    {
        public NHATKIHANHTRINH()
        {
            this.SUKIENs = new List<SUKIEN>();
        }

        public int Ma { get; set; }
        public string MoTa { get; set; }
        public Nullable<int> ChuyenDi { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual CHUYENDI CHUYENDI1 { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
        public virtual ICollection<SUKIEN> SUKIENs { get; set; }
    }
}

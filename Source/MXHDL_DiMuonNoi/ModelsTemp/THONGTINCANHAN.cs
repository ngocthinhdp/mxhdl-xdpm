using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class THONGTINCANHAN
    {
        public string TaiKhoan { get; set; }
        public string Ten { get; set; }
        public string GioiTinh { get; set; }
        public Nullable<System.DateTime> NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string NgheNghiep { get; set; }
        public string TamTrang { get; set; }
        public string DienThoai { get; set; }
        public Nullable<int> AnhDaiDien { get; set; }
        public Nullable<int> AnhBia { get; set; }
        public virtual CANHAN CANHAN { get; set; }
        public virtual HINHANH HINHANH { get; set; }
        public virtual HINHANH HINHANH1 { get; set; }
    }
}

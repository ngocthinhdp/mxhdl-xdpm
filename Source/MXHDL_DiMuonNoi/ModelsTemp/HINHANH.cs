using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class HINHANH
    {
        public HINHANH()
        {
            this.THONGTINCANHANs = new List<THONGTINCANHAN>();
            this.THONGTINCANHANs1 = new List<THONGTINCANHAN>();
            this.THONGTINDOANHNGHIEPs = new List<THONGTINDOANHNGHIEP>();
            this.THONGTINDOANHNGHIEPs1 = new List<THONGTINDOANHNGHIEP>();
        }

        public int Ma { get; set; }
        public string MoTa { get; set; }
        public string TieuDe { get; set; }
        public string DuLieu { get; set; }
        public Nullable<int> KichThuoc { get; set; }
        public Nullable<int> MaAlbum { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual ALBUM ALBUM { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
        public virtual ICollection<THONGTINCANHAN> THONGTINCANHANs { get; set; }
        public virtual ICollection<THONGTINCANHAN> THONGTINCANHANs1 { get; set; }
        public virtual ICollection<THONGTINDOANHNGHIEP> THONGTINDOANHNGHIEPs { get; set; }
        public virtual ICollection<THONGTINDOANHNGHIEP> THONGTINDOANHNGHIEPs1 { get; set; }
    }
}

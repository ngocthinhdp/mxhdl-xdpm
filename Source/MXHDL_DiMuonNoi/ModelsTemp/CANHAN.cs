using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class CANHAN
    {
        public CANHAN()
        {
            this.BANBEs = new List<BANBE>();
            this.BANBEs1 = new List<BANBE>();
            this.DANHGIA_DOANHNGHIEP = new List<DANHGIA_DOANHNGHIEP>();
            this.DANHGIA_DIADIEM = new List<DANHGIA_DIADIEM>();
            this.TAINGUYEN_CANHAN = new List<TAINGUYEN_CANHAN>();
            this.DOANHNGHIEPs = new List<DOANHNGHIEP>();
        }

        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public virtual ICollection<BANBE> BANBEs { get; set; }
        public virtual ICollection<BANBE> BANBEs1 { get; set; }
        public virtual ICollection<DANHGIA_DOANHNGHIEP> DANHGIA_DOANHNGHIEP { get; set; }
        public virtual ICollection<DANHGIA_DIADIEM> DANHGIA_DIADIEM { get; set; }
        public virtual ICollection<TAINGUYEN_CANHAN> TAINGUYEN_CANHAN { get; set; }
        public virtual THONGTINCANHAN THONGTINCANHAN { get; set; }
        public virtual ICollection<DOANHNGHIEP> DOANHNGHIEPs { get; set; }
    }
}

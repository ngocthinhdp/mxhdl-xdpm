using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class TAINGUYEN_CANHAN
    {
        public string TAIKHOAN { get; set; }
        public int MATN { get; set; }
        public string LOAITN { get; set; }
        public Nullable<System.DateTime> THOIGIAN { get; set; }
        public virtual CANHAN CANHAN { get; set; }
        public virtual TAINGUYEN TAINGUYEN { get; set; }
    }
}

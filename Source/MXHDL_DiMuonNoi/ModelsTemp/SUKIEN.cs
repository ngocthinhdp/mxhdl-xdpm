using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class SUKIEN
    {
        public System.DateTime ThoiGian { get; set; }
        public int NhatKi { get; set; }
        public string NoiDung { get; set; }
        public virtual NHATKIHANHTRINH NHATKIHANHTRINH { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class BANBE
    {
        public string CaNhan { get; set; }
        public string BanBe1 { get; set; }
        public string TrangThai { get; set; }
        public virtual CANHAN CANHAN1 { get; set; }
        public virtual CANHAN CANHAN2 { get; set; }
    }
}

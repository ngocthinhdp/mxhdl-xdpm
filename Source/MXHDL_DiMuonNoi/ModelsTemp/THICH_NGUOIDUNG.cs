using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class THICH_NGUOIDUNG
    {
        public string NGUOIDUNG { get; set; }
        public string LOAIND { get; set; }
        public int TAINGUYEN { get; set; }
        public string LOAITN { get; set; }
        public System.DateTime THOIGIAN { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
        public virtual TAINGUYEN TAINGUYEN1 { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class DIADIEM
    {
        public DIADIEM()
        {
            this.DANHGIA_DIADIEM = new List<DANHGIA_DIADIEM>();
            this.CHUYENDIs = new List<CHUYENDI>();
        }

        public int Ma { get; set; }
        public string Ten { get; set; }
        public string DiaChi { get; set; }
        public string MoTa { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual ICollection<DANHGIA_DIADIEM> DANHGIA_DIADIEM { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
        public virtual TOADO_DIADIEM TOADO_DIADIEM { get; set; }
        public virtual ICollection<CHUYENDI> CHUYENDIs { get; set; }
    }
}

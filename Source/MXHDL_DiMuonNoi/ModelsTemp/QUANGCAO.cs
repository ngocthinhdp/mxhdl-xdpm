using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class QUANGCAO
    {
        public int Ma { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public Nullable<int> DoTinCay { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class VIDEO
    {
        public int Ma { get; set; }
        public string MoTa { get; set; }
        public string TieuDe { get; set; }
        public string DuLieu { get; set; }
        public Nullable<System.TimeSpan> ThoiLuong { get; set; }
        public Nullable<int> KichThuoc { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
    }
}

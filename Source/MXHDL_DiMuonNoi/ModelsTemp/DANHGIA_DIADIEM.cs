using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class DANHGIA_DIADIEM
    {
        public string CaNhan { get; set; }
        public int DiaDiem { get; set; }
        public Nullable<byte> Diem { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public virtual CANHAN CANHAN1 { get; set; }
        public virtual DIADIEM DIADIEM1 { get; set; }
    }
}

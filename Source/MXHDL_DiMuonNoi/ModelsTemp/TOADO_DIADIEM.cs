using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class TOADO_DIADIEM
    {
        public int DiaDiem { get; set; }
        public string HoanhDo { get; set; }
        public string TungDo { get; set; }
        public virtual DIADIEM DIADIEM1 { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class NGUOIDUNG
    {
        public NGUOIDUNG()
        {
            this.ALBUMs = new List<ALBUM>();
            this.BAIVIETs = new List<BAIVIET>();
            this.CAMNHAN_NGUOIDUNG = new List<CAMNHAN_NGUOIDUNG>();
            this.CHIASE_NGUOIDUNG = new List<CHIASE_NGUOIDUNG>();
            this.CHUYENDIs = new List<CHUYENDI>();
            this.DAPHUONGTIENs = new List<DAPHUONGTIEN>();
            this.DIADIEMs = new List<DIADIEM>();
            this.HINHANHs = new List<HINHANH>();
            this.NHATKIHANHTRINHs = new List<NHATKIHANHTRINH>();
            this.QUANGCAOs = new List<QUANGCAO>();
            this.THICH_NGUOIDUNG = new List<THICH_NGUOIDUNG>();
            this.VIDEOs = new List<VIDEO>();
        }

        public string TAIKHOAN { get; set; }
        public string LOAIND { get; set; }
        public virtual ICollection<ALBUM> ALBUMs { get; set; }
        public virtual ICollection<BAIVIET> BAIVIETs { get; set; }
        public virtual ICollection<CAMNHAN_NGUOIDUNG> CAMNHAN_NGUOIDUNG { get; set; }
        public virtual ICollection<CHIASE_NGUOIDUNG> CHIASE_NGUOIDUNG { get; set; }
        public virtual ICollection<CHUYENDI> CHUYENDIs { get; set; }
        public virtual ICollection<DAPHUONGTIEN> DAPHUONGTIENs { get; set; }
        public virtual ICollection<DIADIEM> DIADIEMs { get; set; }
        public virtual ICollection<HINHANH> HINHANHs { get; set; }
        public virtual ICollection<NHATKIHANHTRINH> NHATKIHANHTRINHs { get; set; }
        public virtual ICollection<QUANGCAO> QUANGCAOs { get; set; }
        public virtual ICollection<THICH_NGUOIDUNG> THICH_NGUOIDUNG { get; set; }
        public virtual ICollection<VIDEO> VIDEOs { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace MXHDL_DiMuonNoi.Models
{
    public partial class CHUYENDI
    {
        public CHUYENDI()
        {
            this.NHATKIHANHTRINHs = new List<NHATKIHANHTRINH>();
            this.DIADIEMs = new List<DIADIEM>();
        }

        public int Ma { get; set; }
        public string MoTa { get; set; }
        public string NguoiDung { get; set; }
        public string LoaiND { get; set; }
        public virtual NGUOIDUNG NGUOIDUNG1 { get; set; }
        public virtual ICollection<NHATKIHANHTRINH> NHATKIHANHTRINHs { get; set; }
        public virtual ICollection<DIADIEM> DIADIEMs { get; set; }
    }
}

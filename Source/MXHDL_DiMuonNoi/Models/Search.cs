﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MXHDL_DiMuonNoi.Models
{
    public class Search
    {
        [Display(Name = "Từ khóa")]
        public string Keyword { set; get; }
        [Display(Name = "Tùy chọn tìm kiếm")]
        public string SearchOpt { set; get; }
        

        public Search()
        {
            this.Keyword = "";
            this.SearchOpt = "All";
        }
    }
}
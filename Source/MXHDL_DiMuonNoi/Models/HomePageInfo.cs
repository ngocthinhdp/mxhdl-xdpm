﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MXHDL_DiMuonNoi.Models
{
    public class HomePageInfo
    {        
        public DTOLibrary.THONGTIN accInfos { get; set; }
        public List<DTOLibrary.TaiNguyen> lstResource { get; set; }


        public HomePageInfo()
        {            
            accInfos = new DTOLibrary.THONGTINCANHAN();
            lstResource = new List<DTOLibrary.TaiNguyen>();
        }

    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MXHDL_DiMuonNoi.Controllers
{
    public class BusinessUserController : Controller
    {
        //
        // GET: /BusinessUser/
        BUSLibrary.BusinessUser busUser = new BUSLibrary.BusinessUser();

        public ActionResult ChangeInfo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeInfo(DTOLibrary.THONGTINDOANHNGHIEP info)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }
            email = cookie.Value;

            if (!busUser.changeUserInfo(email, info))
            {
                ViewBag.changeInfo = false;
            }
            else
            {
                ViewBag.changeInfo = true;
                return RedirectToAction("Home", "Account");
            }
            return View("ChangeInfo");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MXHDL_DiMuonNoi.Models;
using System.IO;
using DTOLibrary;

namespace MXHDL_DiMuonNoi.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        BUSLibrary.Account busAccount = new BUSLibrary.Account();

        public ActionResult Info(String id)
        {
            
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie != null)
            {
                email = cookie.Value;
                if (email.Equals(id))
                {
                    return RedirectToAction("Home");
                }
            }
            else
            {
                return View("Login");
            }
            if (busAccount.isExistedAccount(id))
            {
                String taikhoan = id;
                DTOLibrary.THONGTIN accInfos = busAccount.getAccountInfos(taikhoan);
                List<DTOLibrary.TaiNguyen> lstTaiNguyen = busAccount.getListResource(taikhoan, DateTime.Now, 10);

                //MXHDL_DiMuonNoi.Models.HomePageInfo homepageInfos = new HomePageInfo();                
                //homepageInfos.accInfos = accInfos;
                //homepageInfos.lstResource = lstTaiNguyen;

                if (email == "")
                {
                    ViewBag.FriendState = "ChuaDangNhap";
                }
                else
                {
                    ViewBag.FriendState = BUSLibrary.Friend.getFriendState(email, taikhoan);
                }

                ViewBag.email = taikhoan;

                ViewBag.userType = busAccount.getUserType(id);
                ViewBag.curUser = accInfos;
                ViewBag.UserInfos = accInfos;
                ViewBag.UserResources = lstTaiNguyen;
                ViewBag.isOwner = false;
                return View("Info");//, homepageInfos);
                
            }
            else
            {
                ViewBag.Status = "Login Failed";
            }
            return View("Login");
        }
        
        public ActionResult Login()
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie != null)
            {
                return RedirectToAction("Home");
            }
            return View();
        }
        
        public ActionResult Home()
        {            
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }            
            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = busAccount.getAccountInfos(email);
            List<DTOLibrary.TaiNguyen> lstTaiNguyen = busAccount.getListResource(email, DateTime.Now, 10);

            //MXHDL_DiMuonNoi.Models.HomePageInfo homepageInfos = new HomePageInfo();
            //homepageInfos.accInfos = accInfos;
            //homepageInfos.lstResource = lstTaiNguyen;


            ViewBag.userType = busAccount.getUserType(email);
            ViewBag.curUser = accInfos;
            ViewBag.UserInfos = accInfos;
            ViewBag.UserResources = lstTaiNguyen;
            ViewBag.isOwner = true;

            return View("Index");//, homepageInfos);            
        }

        [HttpPost]
        public ActionResult Login(DTOLibrary.AccountLogin model)
        {
            if (busAccount.isRegularAccount(new AccountLogin(model.Email, model.Password)))
            {
                String email = model.Email;
                HttpCookie cookie = new HttpCookie("Email", email);
                Response.Cookies.Add(cookie);
                return RedirectToAction("Home");
            }
            else
            {
                ViewBag.Status = "Login Failed";
            }
            return View();
        }

        public ActionResult Logout()
        {
            HttpCookie cookie = new HttpCookie("Email");
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);

            return RedirectToAction("Login");
        }
        [HttpPost]
        public ActionResult Register(DTOLibrary.AccountRegister model)
        {

            if (!busAccount.isExistedAccount(model.Email))
            {
                //Thêm tài khoản
                busAccount.addNewUser(model);
                //

                //DTOLibrary.THONGTINCANHAN accInfos = busAccount.getAccountInfos(model.Email);
                //List<DTOLibrary.TaiNguyen> lstTaiNguyen = busAccount.getListResource(model.Email, DateTime.Now, 10);

                //MXHDL_DiMuonNoi.Models.HomePageInfo homepageInfos = new HomePageInfo();                
                //homepageInfos.accInfos = accInfos;
                //homepageInfos.lstResource = lstTaiNguyen;

                HttpCookie cookie = new HttpCookie("Email", model.Email);
                Response.Cookies.Add(cookie);
                return RedirectToAction("Home");

                //return View("Index", homepageInfos);
            }
            else
            {
                ViewBag.Status = "Register Failed";
                return View("Login");
            }

        }

        public ActionResult ChangeInfo()
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }
            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = busAccount.getAccountInfos(email);

            ViewBag.userType = busAccount.getUserType(email);            
            ViewBag.UserInfos = accInfos;

            return View("ChangeInfo");
        }

       
               

        [HttpPost]
        public ActionResult ChangeAvatar(HttpPostedFileBase file)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];
            
            if (cookie != null)
            {
                email = cookie.Value;
            }
            else
            {
                RedirectToAction("Login");
            }

            MXHDL_DiMuonNoi.Models.HomePageInfo curHompageInfos = new HomePageInfo();            
            curHompageInfos.accInfos = busAccount.getAccountInfos(email);
            curHompageInfos.lstResource = busAccount.getListResource(email, DateTime.Now, 10);
                
           
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {

                int id = busAccount.getNextFileId("HINHANH");
                // extract only the fielname
                var fileName = Path.GetFileName(id + file.FileName);

                // store the file inside ~/App_Data/uploads folder

                var path = Path.Combine(Server.MapPath("~/UserDatas/Images"), fileName);                

                file.SaveAs(path);

                HinhAnh hinh = new HinhAnh();
                hinh.TieuDe = fileName;
                hinh.ThoiGianTao = DateTime.Now;
                hinh.DuLieu = Path.Combine("UserDatas\\Images", fileName);
                hinh.KichThuoc = file.ContentLength;
                int maalbum = busAccount.getIdAlbum(email, "Album tạm");
                if (maalbum != -1)
                {
                    busAccount.addImage(email, hinh, maalbum);
                    busAccount.changeAvatar(email, id);
                }
              
            }
            // redirect back to the index action to show the form once again            
            return RedirectToAction("Home");
        }

        [HttpPost]
        public ActionResult ChangeCover(HttpPostedFileBase file)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie != null)
            {
                email = cookie.Value;
            }
            else
            {
                RedirectToAction("Login");
            }
            MXHDL_DiMuonNoi.Models.HomePageInfo curHompageInfos = new HomePageInfo();            
            curHompageInfos.accInfos = busAccount.getAccountInfos(email);
            curHompageInfos.lstResource = busAccount.getListResource(email, DateTime.Now, 10);


            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                int id = busAccount.getNextFileId("HINHANH");
                // extract only the fielname
                var fileName = Path.GetFileName(id + file.FileName);

                // store the file inside ~/App_Data/uploads folder

                var path = Path.Combine(Server.MapPath("~/UserDatas/Images"), fileName);

                file.SaveAs(path);

                HinhAnh hinh = new HinhAnh();
                hinh.TieuDe = fileName;
                hinh.ThoiGianTao = DateTime.Now;
                hinh.DuLieu = Path.Combine("UserDatas\\Images", fileName);
                hinh.KichThuoc = file.ContentLength;
                int maalbum = busAccount.getIdAlbum(email, "Album tạm");
                if (maalbum != -1)
                {
                    busAccount.addImage(email, hinh, maalbum);
                    busAccount.changeCover(email, id);
                }
              

            }
            // redirect back to the index action to show the form once again            
            return RedirectToAction("Home");
        }
    }
}

    
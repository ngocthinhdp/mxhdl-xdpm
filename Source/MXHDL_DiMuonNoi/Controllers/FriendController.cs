﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTOLibrary;

namespace MXHDL_DiMuonNoi.Controllers
{
    public class FriendController : Controller
    {
        //
        // GET: /Friend/

        public ActionResult Index()
        {            
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return RedirectToAction("Home", "Account");                
            }

            email = cookie.Value;

            List<List<FriendInfo>> lst2 = new List<List<FriendInfo>>();
            lst2.Add(BUSLibrary.Friend.getListFriend(email, "ChoDongY"));
            lst2.Add(BUSLibrary.Friend.getListFriend(email, "BanBe"));
            return View(lst2);
        }

        public ActionResult AddFriend(String id)
        {
            String _email = id;
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return Redirect("/");                
            }

            email = cookie.Value;
            String friendState = BUSLibrary.Friend.getFriendState(email, _email);
            if (friendState == "")
            {
                BUSLibrary.Friend.sendRequest(email, _email);
            }
            else if (friendState == "ChoDongY")
            {
                BUSLibrary.Friend.confirmRequest(email, _email);
            }
            //DayeuCau ->huy?
            return Redirect("/Account/Info/"+_email);
        }

        

    }
}

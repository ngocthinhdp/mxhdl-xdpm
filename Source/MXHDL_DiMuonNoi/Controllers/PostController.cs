﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTOLibrary;

namespace MXHDL_DiMuonNoi.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/
        BUSLibrary.SubmittedPost busRes = new BUSLibrary.SubmittedPost();
        BUSLibrary.RetrievedPost busRes2 = new BUSLibrary.RetrievedPost();
        
        BUSLibrary.Account busAccount = new BUSLibrary.Account();
        public ActionResult Index(String id)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = ( DTOLibrary.THONGTIN)busAccount.getAccountInfos(email);

            ViewBag.AccInfos = accInfos;
            DTOLibrary.BaiViet post = busRes2.getPost(int.Parse(id));
            return View(post);
        }
        public ActionResult New()
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = (DTOLibrary.THONGTIN)busAccount.getAccountInfos(email);

            ViewBag.AccInfos = accInfos;
            return View();
        }
        [HttpPost]
        public ActionResult New(SubmittedPost model)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;
            model.Type = busAccount.getUserType(email);
            model.Email = email;
            bool result = busRes.submitUserPost(email, model);
            return Redirect("/Account/Home");
        }

        
    }
}

﻿using MXHDL_DiMuonNoi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MXHDL_DiMuonNoi.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/
        BUSLibrary.RetrievedPost busRes = new BUSLibrary.RetrievedPost();
        BUSLibrary.Account busAccount = new BUSLibrary.Account();
        public ActionResult Index()
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = (DTOLibrary.THONGTIN)busAccount.getAccountInfos(email);

            ViewBag.AccInfos = accInfos;
            return View();
        }

        public ActionResult Search(Search model)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = (DTOLibrary.THONGTIN)busAccount.getAccountInfos(email);

            ViewBag.AccInfos = accInfos;

            List<DTOLibrary.RetrievedPost> userPosts = busRes.getUserPosts(model.Keyword);
            List<DTOLibrary.RetrievedPost> systemPosts = busRes.getSystemPosts(model.Keyword);
            if (model.SearchOpt == "UserPost")
            {
                systemPosts.Clear();
            }
            if (model.SearchOpt == "SystemPost")
            {
                userPosts.Clear();
            }
            ViewBag.UserPosts = userPosts;
            ViewBag.SystemPosts = systemPosts;
            return View(model);
        }
    }
}

﻿using DTOLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MXHDL_DiMuonNoi.Controllers
{
    public class PlaceController : Controller
    {
        //
        // GET: /Place/

        BUSLibrary.RetrievedPost busRes2 = new BUSLibrary.RetrievedPost();
        BUSLibrary.Account busAccount = new BUSLibrary.Account();
        BUSLibrary.RatingInfo busRating = new BUSLibrary.RatingInfo();
        public ActionResult Index(String id)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;
            DTOLibrary.THONGTIN accInfos = busAccount.getAccountInfos(email);

            ViewBag.AccInfos = accInfos;
            ViewBag.AvgScore = busRating.getAvgScore(id);
            DTOLibrary.RetrievedPost post = busRes2.getSystemPostById(id);
            return View(post);
        }
        [HttpPost]
        public ActionResult Rate(RatingInfo info)
        {
            String email = "";
            var cookie = Request.Cookies["Email"];

            if (cookie == null)
            {
                return View("Login");
            }

            email = cookie.Value;

            info.email = email;
            info.time = DateTime.Now;
            busRating.ratePlace(info);
            return Redirect("Index/" + info.placeId);
        }
    }
}

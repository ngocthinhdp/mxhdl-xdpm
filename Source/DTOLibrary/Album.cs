﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOLibrary
{
    public class Album:TaiNguyen
    {
        public List<HinhAnh> danhsachHinhAnh = new List<HinhAnh>();

        public Album()
        {
            LoaiTN = LOAITN.ALBUM;
        }

        public void ThemHinh(HinhAnh h)
        {
            danhsachHinhAnh.Add(h);
        }

        public override List<HinhAnh> dsHinhMinhHoa()
        {
            List<HinhAnh> result = new List<HinhAnh>();
            if (danhsachHinhAnh.Count == 0)
                return null;
            for (int i = 0; i < (danhsachHinhAnh.Count < 10 ? danhsachHinhAnh.Count : 10); i++)
            {
                result.Add(danhsachHinhAnh[i]);
            }

            return result;
        }
        //public override string ChuyenThanhHtml()
        //{
        //    String result = this.NoiDung.Substring(0, this.NoiDung.Length>1000?1000:this.NoiDung.Length);
        //    "<tr> 				<td valign=\"top\" align=\"left\" colspan=\"2\"> 					<div class=\"album-photos\">" 						<div class=\"photo-strip\">
 												
        //                "</div> 					</div> 				</td> 			</tr>"
        //    for (int i = 0; i < danhsachHinhAnh.Count; i++)
        //    {

        //    }
           

        //}
    }
}

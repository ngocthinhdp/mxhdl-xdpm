﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DTOLibrary
{
    public class AccountRegister
    {        
        [Required]        
        public String Name { get; set; }
        [Required]
        [RegularExpression(@"([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})", ErrorMessage = "Email's format!")]
        public String Email { get; set; }
        [Required]
        public String Password { get; set; }
        
        public bool isBusiness { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOLibrary
{
    public class THONGTIN
    {
        public String TaiKhoan { get; set; }
        public String Ten { get; set; }                
        public String DiaChi { get; set; }
        public String NgheNghiep { get; set; } //Lĩnh vực
        public String TamTrang { get; set; }
        public String DienThoai { get; set; }
        public String AnhDaiDien { get; set; }
        public String AnhBia { get; set; }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOLibrary
{
    public class PhanUngNguoiDung
    {
        public THONGTIN NguoiDung { get; set; }        
        public DateTime ThoiGian { get; set; }

        public PhanUngNguoiDung()
        {
            NguoiDung = new THONGTIN();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOLibrary
{
    public class BaiViet:TaiNguyen
    {
        public BaiViet()
        {
            LoaiTN = LOAITN.BAIVIET;
        }
        public override string TomTat()
        {
            return this.NoiDung.Substring(0, NoiDung.Length > 1000 ? 1000 : NoiDung.Length);
        }        
    }
}

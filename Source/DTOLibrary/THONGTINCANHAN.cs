using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DTOLibrary
{
    public class THONGTINCANHAN : THONGTIN
    {       
        public String GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }                     
        //public virtual CANHAN CANHAN { get; set; }
        //public virtual HINHANH HINHANH { get; set; }
        //public virtual HINHANH HINHANH1 { get; set; }

        public SelectList DSGioiTinh()
        {
            return new SelectList(new List<String>() { "Nam", "Nu", "Khac"
            });
        }
    }
}

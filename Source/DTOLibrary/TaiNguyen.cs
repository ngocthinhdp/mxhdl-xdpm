﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace DTOLibrary
{
    public enum LOAITN { ALBUM, BAIVIET}

    public class TaiNguyen
    {
        public LOAITN LoaiTN { get; set; }
        public DateTime ThoiGianTao { get; set; }
        public String TieuDe{get;set;}
        [AllowHtml]
        public String NoiDung{get;set;}
        public List<Thich> dsThich { get; set; }
        public List<ChiaSe> dsChiaSe { get; set; }
        public List<CamNhan> dsCamNhan { get; set; }

        virtual public String TomTat()
        {
            return null;
        }

        virtual public List<HinhAnh> dsHinhMinhHoa()
        {
            return null;
        }

        virtual public String ChuyenThanhHtml()
        {
            return null;
        }

        public int soLuotThich()
        {
            if (dsThich == null)
                return 0;
            return dsThich.Count;
        }

        public int soLuotChiaSe()
        {
            if (dsChiaSe == null)
                return 0;
            return dsChiaSe.Count;
        }

        public int soLuotCamNhan()
        {
            if (dsCamNhan == null)
                return 0;
            return dsCamNhan.Count;
        }

    }
}

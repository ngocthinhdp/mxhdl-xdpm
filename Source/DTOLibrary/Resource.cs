﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DTOLibrary
{
    public class RetrievedPost
    {
        public String Id { get; set; }
        public String Title { get; set; }
        public String Content { get; set; }
        public String Name { get; set; }
        public String AvatarUrl { get; set; }
        public String Time { get; set; }
    }
    public class SubmittedPost
    {
        public String Type { get; set; }
        public String Email { get; set; }
        [Display(Name = "Tiêu đề")]
        public String Title { get; set; }
        [Display(Name = "Nội dung")]
        [AllowHtml]
        public String Content { get; set; }
    }

    public class RatingInfo
    {
        public String email { get; set; }
        public String placeId { get; set; }
        public String star { get; set; }
        public DateTime time { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOLibrary
{
    public class FriendInfo
    {
        public String Email { get; set; }
        public String Ten { get; set; }
        public String AnhDaiDien { get; set; }
        public String NgheNghiep { get; set; }

        public FriendInfo()
        {

        }
       
    }
}

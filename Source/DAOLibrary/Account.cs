﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTOLibrary;
using System.Data;

namespace DAOLibrary
{
    public class Account
    {

        //public static void addNewUserInfo(String email, String name)
        //{
        //    String query = "INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES('" + email + "', N'" + name + "', 'NAM', GETDATE(), N'', '', N'', 1, 2, N'')";
        //     DataProvider.getInstance().NonQueryDatabase(query);
        //}

        public String getUserType(String email)
        {
            String query = "SELECT NGUOIDUNG.LOAIND FROM NGUOIDUNG WHERE NGUOIDUNG.TAIKHOAN = '" + email + "'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    return dt.Tables[0].Rows[0][0].ToString();
                }
            }

            return null;

        }
        public DTOLibrary.THONGTIN getAccountInfos(String email, String password)
        {            
            String loaind = this.getUserType(email);
            if (loaind == null)
                return null;

            String query = "";
            if (loaind.Equals("CANHAN"))
                query = "SELECT THONGTINCANHAN.Ten, THONGTINCANHAN.GioiTinh, THONGTINCANHAN.NgaySinh, THONGTINCANHAN.DiaChi, THONGTINCANHAN.NgheNghiep, THONGTINCANHAN.TamTrang, THONGTINCANHAN.DienThoai, ANHDAIDIEN.DuLieu AS AnhDaiDien, ANHBIA.DuLieu AS AnhBia FROM CANHAN INNER JOIN THONGTINCANHAN ON CANHAN.TaiKhoan = THONGTINCANHAN.TaiKhoan INNER JOIN HINHANH ANHBIA ON THONGTINCANHAN.AnhBia = ANHBIA.Ma INNER JOIN HINHANH ANHDAIDIEN ON THONGTINCANHAN.AnhDaiDien=ANHDAIDIEN.Ma  WHERE CANHAN.TaiKhoan LIKE '" + email + "' AND CANHAN.MatKhau LIKE '" + password + "'";
            else //Doanh nghiep *chu y
                query = "SELECT THONGTINDOANHNGHIEP.Ten, THONGTINDOANHNGHIEP.DiaChi, THONGTINDOANHNGHIEP.NgheNghiep, THONGTINDOANHNGHIEP.TamTrang, THONGTINDOANHNGHIEP.DienThoai, ANHDAIDIEN.DuLieu AS AnhDaiDien, ANHBIA.DuLieu AS AnhBia, THONGTINDOANHNGHIEP.MoTa FROM DOANHNGHIEP INNER JOIN THONGTINDOANHNGHIEP ON DOANHNGHIEP.TaiKhoan = THONGTINDOANHNGHIEP.TaiKhoan INNER JOIN HINHANH ANHBIA ON THONGTINDOANHNGHIEP.AnhBia = ANHBIA.Ma INNER JOIN HINHANH ANHDAIDIEN ON THONGTINDOANHNGHIEP.AnhDaiDien=ANHDAIDIEN.Ma  WHERE DOANHNGHIEP.TaiKhoan LIKE '" + email + "' AND MatKhau LIKE '" + password + "'";

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                   
                    if (loaind.Equals("CANHAN"))
                    {
                        DTOLibrary.THONGTINCANHAN acc = new THONGTINCANHAN();
                        acc.Ten = dt.Tables[0].Rows[0]["Ten"].ToString();
                        acc.DiaChi = dt.Tables[0].Rows[0]["DiaChi"].ToString();
                        acc.NgheNghiep = dt.Tables[0].Rows[0]["NgheNghiep"].ToString();
                        acc.TamTrang = dt.Tables[0].Rows[0]["TamTrang"].ToString();
                        acc.DienThoai = dt.Tables[0].Rows[0]["DienThoai"].ToString();
                        acc.AnhDaiDien = dt.Tables[0].Rows[0]["AnhDaiDien"].ToString();
                        acc.AnhBia = dt.Tables[0].Rows[0]["AnhBia"].ToString();
                                          
                        acc.GioiTinh = dt.Tables[0].Rows[0]["GioiTinh"].ToString();                    
                        acc.NgaySinh = DateTime.Parse(dt.Tables[0].Rows[0]["NgaySinh"].ToString());
                        return acc;
                    }
                    else
                    {
                        DTOLibrary.THONGTINDOANHNGHIEP acc = new THONGTINDOANHNGHIEP();
                        acc.Ten = dt.Tables[0].Rows[0]["Ten"].ToString();
                        acc.DiaChi = dt.Tables[0].Rows[0]["DiaChi"].ToString();
                        acc.NgheNghiep = dt.Tables[0].Rows[0]["NgheNghiep"].ToString();
                        acc.TamTrang = dt.Tables[0].Rows[0]["TamTrang"].ToString();
                        acc.DienThoai = dt.Tables[0].Rows[0]["DienThoai"].ToString();
                        acc.AnhDaiDien = dt.Tables[0].Rows[0]["AnhDaiDien"].ToString();
                        acc.AnhBia = dt.Tables[0].Rows[0]["AnhBia"].ToString();
                        
                        acc.MoTa = dt.Tables[0].Rows[0]["MoTa"].ToString();
                        return acc;
                    }
                    //..
                    
                }
            }

            return null;
        }

        public DTOLibrary.THONGTIN getAccountInfos(String email)
        {
            String loaind = this.getUserType(email);
            if (loaind == null)
                return null;

            String query = "";
            if (loaind.Equals("CANHAN"))
                query = "SELECT THONGTINCANHAN.Ten, THONGTINCANHAN.GioiTinh, THONGTINCANHAN.NgaySinh, THONGTINCANHAN.DiaChi, THONGTINCANHAN.NgheNghiep, THONGTINCANHAN.TamTrang, THONGTINCANHAN.DienThoai, ANHDAIDIEN.DuLieu AS AnhDaiDien, ANHBIA.DuLieu AS AnhBia FROM CANHAN INNER JOIN THONGTINCANHAN ON CANHAN.TaiKhoan = THONGTINCANHAN.TaiKhoan INNER JOIN HINHANH ANHBIA ON THONGTINCANHAN.AnhBia = ANHBIA.Ma INNER JOIN HINHANH ANHDAIDIEN ON THONGTINCANHAN.AnhDaiDien=ANHDAIDIEN.Ma  WHERE CANHAN.TaiKhoan LIKE '" + email + "'";
            else //Doanh nghiep *chu y
                query = "SELECT THONGTINDOANHNGHIEP.Ten, THONGTINDOANHNGHIEP.DiaChi, THONGTINDOANHNGHIEP.NgheNghiep, THONGTINDOANHNGHIEP.TamTrang, THONGTINDOANHNGHIEP.DienThoai, ANHDAIDIEN.DuLieu AS AnhDaiDien, ANHBIA.DuLieu AS AnhBia, THONGTINDOANHNGHIEP.MoTa FROM DOANHNGHIEP INNER JOIN THONGTINDOANHNGHIEP ON DOANHNGHIEP.TaiKhoan = THONGTINDOANHNGHIEP.TaiKhoan INNER JOIN HINHANH ANHBIA ON THONGTINDOANHNGHIEP.AnhBia = ANHBIA.Ma INNER JOIN HINHANH ANHDAIDIEN ON THONGTINDOANHNGHIEP.AnhDaiDien=ANHDAIDIEN.Ma  WHERE DOANHNGHIEP.TaiKhoan LIKE '" + email + "'";

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {

                    if (loaind.Equals("CANHAN"))
                    {
                        DTOLibrary.THONGTINCANHAN acc = new THONGTINCANHAN();
                        acc.Ten = dt.Tables[0].Rows[0]["Ten"].ToString();
                        acc.DiaChi = dt.Tables[0].Rows[0]["DiaChi"].ToString();
                        acc.NgheNghiep = dt.Tables[0].Rows[0]["NgheNghiep"].ToString();
                        acc.TamTrang = dt.Tables[0].Rows[0]["TamTrang"].ToString();
                        acc.DienThoai = dt.Tables[0].Rows[0]["DienThoai"].ToString();
                        acc.AnhDaiDien = dt.Tables[0].Rows[0]["AnhDaiDien"].ToString();
                        acc.AnhBia = dt.Tables[0].Rows[0]["AnhBia"].ToString();

                        acc.GioiTinh = dt.Tables[0].Rows[0]["GioiTinh"].ToString();
                        acc.NgaySinh = DateTime.Parse(dt.Tables[0].Rows[0]["NgaySinh"].ToString());
                        return acc;
                    }
                    else
                    {
                        DTOLibrary.THONGTINDOANHNGHIEP acc = new THONGTINDOANHNGHIEP();
                        acc.Ten = dt.Tables[0].Rows[0]["Ten"].ToString();
                        acc.DiaChi = dt.Tables[0].Rows[0]["DiaChi"].ToString();
                        acc.NgheNghiep = dt.Tables[0].Rows[0]["NgheNghiep"].ToString();
                        acc.TamTrang = dt.Tables[0].Rows[0]["TamTrang"].ToString();
                        acc.DienThoai = dt.Tables[0].Rows[0]["DienThoai"].ToString();
                        acc.AnhDaiDien = dt.Tables[0].Rows[0]["AnhDaiDien"].ToString();
                        acc.AnhBia = dt.Tables[0].Rows[0]["AnhBia"].ToString();

                        acc.MoTa = dt.Tables[0].Rows[0]["MoTa"].ToString();
                        return acc;
                    }
                    //..

                }
            }

            return null;
        }


        public DTOLibrary.BaiViet getPost(int id)
        {
            DTOLibrary.BaiViet result = null;
            String query = "SELECT * FROM BAIVIET WHERE BAIVIET.Ma = "+id;
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    result = new BaiViet();
                    result.TieuDe = dt.Tables[0].Rows[0]["TieuDe"].ToString();
                    result.NoiDung = dt.Tables[0].Rows[0]["NoiDung"].ToString();
                    return result;
                }
            }

            return null;           
        }
        public DTOLibrary.Album getAlbum(int id)
        {
            DTOLibrary.Album result = null;
            String query = "SELECT * FROM ALBUM WHERE ALBUM.Ma = " + id;
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    result = new Album();
                    result.TieuDe = dt.Tables[0].Rows[0]["TieuDe"].ToString();
                    result.NoiDung = dt.Tables[0].Rows[0]["MoTa"].ToString();
                    List<HinhAnh> lst = getImageByAlbumID(id);
                    if (lst != null)
                    {
                        foreach (HinhAnh hinhanh in lst)
                            result.ThemHinh(hinhanh);
                    }
                    return result;
                }
            }

            return null;
        }
        public List<DTOLibrary.HinhAnh> getImageByAlbumID(int id)
        {
            List<DTOLibrary.HinhAnh> lst = null;

            String query = "SELECT * FROM HINHANH WHERE HINHANH.MaAlbum = "+id;

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    lst = new List<HinhAnh>();
                    for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                    {
                        HinhAnh hinhanh = new HinhAnh();
                        hinhanh.TieuDe = dt.Tables[0].Rows[i]["TieuDe"].ToString();
                        hinhanh.NoiDung = dt.Tables[0].Rows[i]["MoTa"].ToString();
                        hinhanh.DuLieu = dt.Tables[0].Rows[i]["DuLieu"].ToString();
                        hinhanh.KichThuoc = int.Parse(dt.Tables[0].Rows[i]["KichThuoc"].ToString());

                        lst.Add(hinhanh);
                    }

                    //..
                    return lst;
                }
            }

            return null;
        }
        public List<DTOLibrary.TaiNguyen> getListResource(String account, DateTime from, int nums)
        {
            List<DTOLibrary.TaiNguyen> lst = null;

            String loaind = this.getUserType(account);
            String query = "";
            if (loaind.Equals("CANHAN"))
            {
                query = "SELECT TOP " + nums + " * FROM TAINGUYEN_CANHAN WHERE TAINGUYEN_CANHAN.TAIKHOAN = '" + account + "' AND (TAINGUYEN_CANHAN.LOAITN = 'BAIVIET' OR TAINGUYEN_CANHAN.LOAITN = 'ALBUM') AND TAINGUYEN_CANHAN.THOIGIAN < CONVERT(datetime, '" + from.ToString() + "') ORDER BY(TAINGUYEN_CANHAN.THOIGIAN) DESC";                        
            }
            else
            {
                query = "SELECT TOP " + nums + " * FROM TAINGUYEN_DOANHNGHIEP WHERE TAINGUYEN_DOANHNGHIEP.TAIKHOAN = '" + account + "' AND (TAINGUYEN_DOANHNGHIEP.LOAITN = 'BAIVIET' OR TAINGUYEN_DOANHNGHIEP.LOAITN = 'ALBUM') AND TAINGUYEN_DOANHNGHIEP.THOIGIAN < CONVERT(datetime, '" + from.ToString() + "') ORDER BY(TAINGUYEN_DOANHNGHIEP.THOIGIAN) DESC";                        
            }
            

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    lst = new List<TaiNguyen>();
                    for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                    {
                        int id = int.Parse(dt.Tables[0].Rows[i]["MATN"].ToString());
                        String loaitn = dt.Tables[0].Rows[i]["LOAITN"].ToString();
                        TaiNguyen tainguyen = null;
                        
                        if (loaitn.Equals("BAIVIET"))
                        {
                            tainguyen = getPost(id);
                            tainguyen.dsCamNhan = getListComment(id, "BAIVIET");
                            tainguyen.dsChiaSe = getListShare(id, "BAIVIET");
                            tainguyen.dsThich = getListLike(id, "BAIVIET");
                        }
                        else if (loaitn.Equals("ALBUM"))
                        {
                            tainguyen = getAlbum(id);
                            tainguyen.dsCamNhan = getListComment(id, "ALBUM");
                            tainguyen.dsChiaSe = getListShare(id, "ALBUM");
                            tainguyen.dsThich = getListLike(id, "ALBUM");
                        }
                        if (tainguyen != null)
                        {
                            tainguyen.ThoiGianTao = DateTime.Parse(dt.Tables[0].Rows[i]["THOIGIAN"].ToString());
                            lst.Add(tainguyen);
                        }
                    }
                    
                    //..
                    return lst;
                }
            }

            return null;
        }

        public List<CamNhan> getListComment(int resourceID, String typeResource)
        {
            List<DTOLibrary.CamNhan> lst = null;

            String query = "SELECT * FROM CAMNHAN_NGUOIDUNG WHERE CAMNHAN_NGUOIDUNG.TAINGUYEN = "+resourceID+" AND CAMNHAN_NGUOIDUNG.LOAITN = '"+typeResource+"'ORDER BY (CAMNHAN_NGUOIDUNG.THOIGIAN) ASC ";

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    lst = new List<CamNhan>();
                    for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                    {
                        CamNhan camnhan = new CamNhan();
                        String taikhoan = dt.Tables[0].Rows[i]["NguoiDung"].ToString();
                        camnhan.NguoiDung = getAccountInfos(taikhoan);                        
                        camnhan.ThoiGian = DateTime.Parse(dt.Tables[0].Rows[i]["ThoiGian"].ToString());
                        camnhan.NoiDung = dt.Tables[0].Rows[i]["CamNhan"].ToString();                        

                        lst.Add(camnhan);
                    }

                    //..
                    return lst;
                }
            }

            return null;
        }

        public List<Thich> getListLike(int resourceID, String typeResource)
        {
            List<DTOLibrary.Thich> lst = null;

            String query = "SELECT * FROM THICH_NGUOIDUNG WHERE THICH_NGUOIDUNG.TAINGUYEN = " + resourceID + " AND THICH_NGUOIDUNG.LOAITN = '" + typeResource + "'ORDER BY (THICH_NGUOIDUNG.THOIGIAN) ASC ";

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    lst = new List<Thich>();
                    for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                    {
                        Thich thich = new Thich();
                        String taikhoan = dt.Tables[0].Rows[i]["NguoiDung"].ToString();
                        thich.NguoiDung = getAccountInfos(taikhoan);
                        thich.ThoiGian = DateTime.Parse(dt.Tables[0].Rows[i]["ThoiGian"].ToString());                        

                        lst.Add(thich);
                    }

                    //..
                    return lst;
                }
            }

            return null;
        }

        public List<ChiaSe> getListShare(int resourceID, String typeResource)
        {
            List<DTOLibrary.ChiaSe> lst = null;

            String query = "SELECT * FROM THICH_NGUOIDUNG WHERE THICH_NGUOIDUNG.TAINGUYEN = " + resourceID + " AND THICH_NGUOIDUNG.LOAITN = '" + typeResource + "'ORDER BY (THICH_NGUOIDUNG.THOIGIAN) ASC ";

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    lst = new List<ChiaSe>();
                    for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                    {
                        ChiaSe chiase = new ChiaSe();
                        String taikhoan = dt.Tables[0].Rows[i]["NguoiDung"].ToString();
                        chiase.NguoiDung = getAccountInfos(taikhoan);
                        chiase.ThoiGian = DateTime.Parse(dt.Tables[0].Rows[i]["ThoiGian"].ToString());

                        lst.Add(chiase);
                    }

                    //..
                    return lst;
                }
            }

            return null;
        }

        public int getMaxFileId(String type)
        {
            String query = "SELECT MAX(MA) FROM "+type;

            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    return int.Parse(dt.Tables[0].Rows[0][0].ToString());
                }
            }

            return 0;
        }

        public void changeAvatar(String email, int id)
        {
            String userType = this.getUserType(email);
            String query = "";
            if (userType.Equals("CANHAN"))
            {
                query = "UPDATE THONGTINCANHAN SET AnhDaiDien = " + id + " WHERE THONGTINCANHAN.TaiKhoan = '" + email + "'";
            }
            else
            {
                query = "UPDATE THONGTINDOANHNGHIEP SET AnhDaiDien = " + id + " WHERE THONGTINDOANHNGHIEP.TaiKhoan = '" + email + "'";
            }
            

            DataProvider.getInstance().NonQueryDatabase(query);
        }

        public void changeCover(String email, int id)
        {
            String userType = this.getUserType(email);
            String query = "";
            if (userType.Equals("CANHAN"))
            {
                query = "UPDATE THONGTINCANHAN SET AnhBia = " + id + " WHERE THONGTINCANHAN.TaiKhoan = '" + email + "'";
            }
            else
            {
                query = "UPDATE THONGTINDOANHNGHIEP SET AnhBia = " + id + " WHERE THONGTINDOANHNGHIEP.TaiKhoan = '" + email + "'";
            }

            DataProvider.getInstance().NonQueryDatabase(query);
        }

        public void addNewUser(DTOLibrary.AccountRegister acc)
        {
            String query = "";
            if (acc.isBusiness)
                query = "INSERT INTO DOANHNGHIEP(Taikhoan, Matkhau) VALUES('" + acc.Email + "', '" + acc.Password + "') INSERT INTO THONGTINDOANHNGHIEP(TaiKhoan, Ten, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang, MoTa)  VALUES(N'" + acc.Email + "', N'" + acc.Name + "', N'Địa chỉ', 'Số điện thoại', N'Nghề nghiệp', 1, 2, N'Tâm trạng của tôi', N'Mô tả')";
            else
                query = "INSERT INTO CANHAN(Taikhoan, Matkhau) VALUES('" + acc.Email + "', '" + acc.Password + "') INSERT INTO THONGTINCANHAN(TaiKhoan, Ten, GioiTinh, NgaySinh, DiaChi, DienThoai, NgheNghiep, AnhDaiDien, AnhBia, TamTrang)  VALUES(N'" + acc.Email + "', N'" + acc.Name + "', 'NAM', '01/09/1992', N'Địa chỉ', 'Số điện thoại', N'Nghề nghiệp', 1, 2, N'Tâm trạng của tôi')";
            DataProvider.getInstance().NonQueryDatabase(query);
        }

        public void addImage(String email, HinhAnh hinh, int maalbum)
        {
            String query = "INSERT INTO HINHANH(TieuDe, MoTa, KichThuoc, DuLieu, MaAlbum,NguoiDung, LoaiND) VALUES(N'" + hinh.TieuDe + "', N'', " + hinh.KichThuoc + ", '" + hinh.DuLieu + "', " + maalbum + ", '" + email + "', '" + this.getUserType(email) + "')"; //TẠM THỜI ĐỂ NGƯỜI DÙNG CÁ NHÂN
            DataProvider.getInstance().NonQueryDatabase(query);
        }

        public int getIdAlbum(String email, String nameAlbum)
        {
            String userType = this.getUserType(email);
            String query = "";
            if (userType.Equals("CANHAN"))
            {
                query = "SELECT ALBUM.Ma FROM ALBUM JOIN TAINGUYEN_CANHAN ON (TAINGUYEN_CANHAN.LOAITN='ALBUM' AND ALBUM.Ma = TAINGUYEN_CANHAN.MATN) WHERE TAINGUYEN_CANHAN.TAIKHOAN = '" + email + "' AND ALBUM.TieuDe = N'" + nameAlbum + "'";
            }
            else
            {
                query = "SELECT ALBUM.Ma FROM ALBUM JOIN TAINGUYEN_DOANHNGHIEP ON (TAINGUYEN_DOANHNGHIEP.LOAITN='ALBUM' AND ALBUM.Ma = TAINGUYEN_DOANHNGHIEP.MATN) WHERE TAINGUYEN_DOANHNGHIEP.TAIKHOAN = '" + email + "' AND ALBUM.TieuDe = N'" + nameAlbum + "'";
            }            
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    return int.Parse(dt.Tables[0].Rows[0][0].ToString());
                }
            }

            return -1;
        }

        public void addAlbum(String email, Album alb)
        {
            String query = "INSERT INTO ALBUM(MoTa, TieuDe, NguoiDung, LoaiND) VALUES(N'" + alb.NoiDung + "', N'" + alb.TieuDe + "', '" + email + "', '"+this.getUserType(email)+"')"; //TẠM THỜI ĐỂ NGƯỜI DÙNG CÁ NHÂN
            DataProvider.getInstance().NonQueryDatabase(query);
        }


        public bool changeUserInfo(String email, THONGTIN info)
        {

            String userType = this.getUserType(email);
            String query = "";
            if (userType.Equals("CANHAN"))
            {
                THONGTINCANHAN pinfo = (THONGTINCANHAN)info;
                query = "UPDATE THONGTINCANHAN SET Ten = N'" + pinfo.Ten + "', DiaChi = N'" + pinfo.DiaChi + "', GioiTinh = N'" + pinfo.GioiTinh + "', NgaySinh = N'" + pinfo.NgaySinh + "', DienThoai = N'" + pinfo.DienThoai + "', NgheNghiep = N'" + pinfo.NgheNghiep + "' WHERE THONGTINCANHAN.TaiKhoan = N'" + email + "'";
            }
            else
            {
                THONGTINDOANHNGHIEP binfo = (THONGTINDOANHNGHIEP)info;
                query = "UPDATE THONGTINDOANHNGHIEP SET Ten = N'" + binfo.Ten + "', DiaChi = N'" + binfo.DiaChi + "', MoTa = N'" + binfo.MoTa  + "', DienThoai = N'" + binfo.DienThoai + "', NgheNghiep = N'" + binfo.NgheNghiep + "' WHERE THONGTINDOANHNGHIEP.TaiKhoan = N'" + email + "'";
            }

            DataProvider.getInstance().NonQueryDatabase(query);
            return true;

            
        }
    }
    
}

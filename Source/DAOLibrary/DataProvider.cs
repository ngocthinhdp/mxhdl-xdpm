﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAOLibrary
{
    public class DataProvider
    {
        public static String connectionString = "Data Source=ADMIN-PC;Initial Catalog=MXH_DULICH;Integrated Security=True";
        private static DataProvider instance = null;

        SqlConnection mConDatabase;
        SqlCommand mComDatabase;
        SqlDataAdapter mDataAdapter;
        DataSet _dataSet;
        
        private DataProvider(String connectionString)
        {
            connectionString += ";Asynchronous Processing=true";
            connectionString += ";MultipleActiveResultSets=true";
            
            mConDatabase = new SqlConnection(connectionString);
            mConDatabase.Open();
        }

        public static DataProvider getInstance()
        {
            if (instance == null)            
                instance = new DataProvider(connectionString);

            return instance;
        }

        //Query
        public DataSet QueryDatabase(String _query)
        {
            DataSet dataSet = new DataSet();
            this.mComDatabase = new SqlCommand(_query, this.mConDatabase);
            this.mDataAdapter = new SqlDataAdapter(this.mComDatabase);
            try
            {
                this.mDataAdapter.Fill(dataSet);
                return dataSet;
            }
            catch (Exception ex)
            {

                return dataSet;//null
            }
            
            
        }
        //Non - query
        public bool NonQueryDatabase(String _query)
        {
            if (!(mConDatabase.State == ConnectionState.Open))
            {
                mConDatabase.Open();
            }
            this.mComDatabase = new SqlCommand(_query, this.mConDatabase);
            try
            {
                this.mComDatabase.BeginExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DTOLibrary;

namespace DAOLibrary
{
    public class Resource
    {
        private static readonly string[] VietnameseSigns = new string[]
{

"aAeEoOuUiIdDyY",

"áàạảãâấầậẩẫăắằặẳẵ",

"ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

"éèẹẻẽêếềệểễ",

"ÉÈẸẺẼÊẾỀỆỂỄ",

"óòọỏõôốồộổỗơớờợởỡ",

"ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

"úùụủũưứừựửữ",

"ÚÙỤỦŨƯỨỪỰỬỮ",

"íìịỉĩ",

"ÍÌỊỈĨ",

"đ",

"Đ",

"ýỳỵỷỹ",

"ÝỲỴỶỸ"

};

        public static string locDau(string str)
        {

            for (int i = 1; i < VietnameseSigns.Length; i++)
            {

                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

            }

            return str;

        }
        public List<RetrievedPost> getUserPosts(String keyword)
        {
            List<RetrievedPost> result = new List<RetrievedPost>();
            DTOLibrary.RetrievedPost post = null;
            keyword = keyword.ToLower();
            String query = "SELECT BAIVIET.Ma, BAIVIET.TieuDe, BAIVIET.NoiDung, THONGTINCANHAN.Ten, THONGTINCANHAN.AnhDaiDien, TAINGUYEN_CANHAN.THOIGIAN FROM BAIVIET JOIN TAINGUYEN_CANHAN ON BAIVIET.MA = TAINGUYEN_CANHAN.MATN JOIN CANHAN ON TAINGUYEN_CANHAN.TAIKHOAN = CANHAN.TaiKhoan JOIN THONGTINCANHAN ON CANHAN.TaiKhoan = THONGTINCANHAN.TAIKHOAN WHERE dbo.fuConvertToUnsign2(NoiDung) LIKE '%" + locDau(keyword).ToLower() + "%'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);

            if (dt.Tables.Count != 0)
            {
                foreach (DataRow x in dt.Tables[0].Rows)
                {
                    post = new RetrievedPost();
                    post.Id = x[0].ToString();
                    post.Title = x[1].ToString();
                    post.Content = x[2].ToString();
                    post.Name = x[3].ToString();
                    post.AvatarUrl = x[4].ToString();
                    post.Time = x[5].ToString();
                    result.Add(post);
                }
                return result;
            }

            return null;
        }

        public List<RetrievedPost> getSystemPosts(String keyword)
        {
            List<RetrievedPost> result = new List<RetrievedPost>();
            DTOLibrary.RetrievedPost post = null;
            keyword = keyword.ToLower();
            String query = "SELECT * FROM DIADIEM WHERE dbo.fuConvertToUnsign2(MoTa) LIKE '%" + locDau(keyword).ToLower() + "%'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);

            if (dt.Tables.Count != 0)
            {
                foreach (DataRow x in dt.Tables[0].Rows)
                {
                    post = new RetrievedPost();
                    post.Title = x[1].ToString();
                    post.Content = x[3].ToString();
                    result.Add(post);
                }
                return result;
            }

            return null;
        }

        

        public bool submitUserPost(DTOLibrary.SubmittedPost post)
        {
            String query = "INSERT INTO BAIVIET VALUES(N'" + post.Title + "', N'" + post.Content + "') ";
            if (DataProvider.getInstance().NonQueryDatabase(query) == true)
            {
                String query2 = "SELECT MAX(Ma) FROM BAIVIET";
                DataSet dt = DataProvider.getInstance().QueryDatabase(query2);
                if (dt != null)
                {
                    string id = dt.Tables[0].Rows[0][0].ToString();
                    String query3 = "INSERT INTO TAINGUYEN_CANHAN values ('" + post.Email + "', " + id + ", '" + post.Type + "', GetDate())";
                    return DataProvider.getInstance().NonQueryDatabase(query3);
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        public bool submitUserPost(String email, DTOLibrary.SubmittedPost post)
        {
            String query = "INSERT INTO BAIVIET VALUES(N'" + post.Title + "', N'" + post.Content + "', '"+email+"', '" + post.Type + "') ";

            if (DataProvider.getInstance().NonQueryDatabase(query) == true)
            {
                return true;
                //String query2 = "SELECT MAX(Ma) FROM BAIVIET";
                //DataSet dt = DataProvider.getInstance().QueryDatabase(query2);
                //if (dt != null)
                //{
                //    string id = dt.Tables[0].Rows[0][0].ToString();
                //    String query3 = "INSERT INTO TAINGUYEN_CANHAN values ('" + post.Email + "', " + id + ", 'BAIVIET', GetDate())";
                //    return DataProvider.getInstance().NonQueryDatabase(query3);
                //}
                //else
                //{
                //    return false;
                //}
            }
            return false;
        }

        public RetrievedPost getUserPostById(String id)
        {
            String query = "SELECT * FROM BAIVIET WHERE Ma =" + id;
            DTOLibrary.RetrievedPost post = null;
            DataSet dt = new DataSet();
            dt =   DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables[0].Rows.Count != 0)
            {
                post = new RetrievedPost();
                post.Id = dt.Tables[0].Rows[0][0].ToString();
                post.Title = dt.Tables[0].Rows[0][1].ToString();
                post.Content = dt.Tables[0].Rows[0][2].ToString();
                return post;
            }
            return null;
        }

        public RetrievedPost getSystemPostById(String id)
        {
            DTOLibrary.RetrievedPost post = null;
            String query = "SELECT * FROM DIADIEM WHERE Ma = '" + id + "'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables[0].Rows.Count != 0)
            {
                post = new RetrievedPost();
                post.Id = dt.Tables[0].Rows[0][0].ToString();
                post.Title = dt.Tables[0].Rows[0][1].ToString();
                post.Content = dt.Tables[0].Rows[0][3].ToString();
                return post;
            }
            return null;
        }

        public bool ratePlace(RatingInfo info)
        {
            String query0 = "SELECT * FROM DANHGIA_DIADIEM WHERE CaNhan='" + info.email + "' AND DiaDiem='" + info.placeId + "'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query0);
            if (dt.Tables[0].Rows.Count == 0)
            {
                String query = "INSERT INTO DANHGIA_DIADIEM VALUES('" + info.email + "', '" + info.placeId + "', '" + info.star + "', '" + info.time + "')";
                if (DataProvider.getInstance().NonQueryDatabase(query) == true)
                {
                    return true;
                }
            }
            else
            {
                String query = "UPDATE DANHGIA_DIADIEM SET Diem ='" + info.star + "', ThoiGian='" + info.time + "' WHERE CaNhan='" + info.email + "' AND DiaDiem='" + info.placeId + "'";
                if (DataProvider.getInstance().NonQueryDatabase(query) == true)
                {
                    return true;
                }
            }

            
            return false;
        }

        public double getAvgScore(String placeId)
        {
            String query = "SELECT AVG(CAST(DIEM AS FLOAT)) FROM DANHGIA_DIADIEM WHERE DiaDiem='" + placeId + "'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);

            if (dt.Tables.Count != 0)
            {
                try
                {
                    return double.Parse(dt.Tables[0].Rows[0][0].ToString());
                }
                catch
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
    }
}

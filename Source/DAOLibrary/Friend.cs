﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DTOLibrary;

namespace DAOLibrary
{
    public class Friend
    {
        public static List<DTOLibrary.FriendInfo> getListFriend(String email, String state)
        {
            List<DTOLibrary.FriendInfo> lst;
            String query = "SELECT BANBE.BanBe AS Email, THONGTINCANHAN.Ten, THONGTINCANHAN.NgheNghiep, HINHANH.DuLieu FROM BANBE INNER JOIN THONGTINCANHAN ON THONGTINCANHAN.TaiKhoan=BANBE.BanBe INNER JOIN HINHANH ON THONGTINCANHAN.AnhDaiDien=HINHANH.Ma WHERE BANBE.CaNhan = '" + email + "' AND BANBE.TrangThai='" + state + "'";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    lst = new List<DTOLibrary.FriendInfo>();
                    for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                    {
                        FriendInfo fi = new FriendInfo();
                        fi.Email = dt.Tables[0].Rows[i]["Email"].ToString();
                        fi.Ten = dt.Tables[0].Rows[i]["Ten"].ToString();
                        fi.NgheNghiep = dt.Tables[0].Rows[i]["NgheNghiep"].ToString();
                        fi.AnhDaiDien = dt.Tables[0].Rows[i]["DuLieu"].ToString();                        
                        lst.Add(fi);
                    }

                    //..
                    return lst;
                }
            }

            return null;
        }

        public static String getFriendState(String email1, String email2)
        {
            String query = "SELECT TrangThai FROM BANBE WHERE (BANBE.CaNhan = '"+email1+"' AND BANBE.BanBe = '"+email2+"')";
            DataSet dt = DataProvider.getInstance().QueryDatabase(query);
            if (dt.Tables.Count != 0)
            {
                if (dt.Tables[0].Rows.Count != 0)
                {
                    return dt.Tables[0].Rows[0][0].ToString();
                }
            }

            return "";
        }

        public static void sendRequest(String email1, String email2)
        {
            String query = "INSERT INTO BANBE(CaNhan, BanBe, TrangThai) VALUES('"+email1+"', '"+email2+"', 'DaYeuCau')";
            DataProvider.getInstance().NonQueryDatabase(query);
            query = "INSERT INTO BANBE(CaNhan, BanBe, TrangThai) VALUES('" + email2 + "', '" + email1 + "', 'ChoDongY')";
            DataProvider.getInstance().NonQueryDatabase(query);
                       
        }

        public static void confirmRequest(String email1, String email2)
        {
            DAOLibrary.Friend.changeState(email1, email2, "BanBe");
            DAOLibrary.Friend.changeState(email2, email1, "BanBe");
        }

        public static void changeState(String email1, String email2, String state)
        {
            String query = "UPDATE BANBE SET TrangThai = '"+state+"' WHERE (BANBE.CaNhan = '"+email1+"' AND BANBE.BanBe = '"+email2+"')";
            DataProvider.getInstance().NonQueryDatabase(query);
        }
    }
}

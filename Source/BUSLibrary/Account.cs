﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTOLibrary;

namespace BUSLibrary
{
    public class Account
    {
        DAOLibrary.Account daoAccount = new DAOLibrary.Account();

        
        public String getUserType(String email)
        {
            return daoAccount.getUserType(email);

        }
        public DTOLibrary.THONGTIN getAccountInfos(String email)
        {
            
            DTOLibrary.THONGTIN acc = daoAccount.getAccountInfos(email);

            return acc;
        }

        public bool isExistedAccount(String email)
        {
            String accType = daoAccount.getUserType(email);

            if (accType == null)
            {
                return false;
            }
            return true;
        }

        public bool isRegularAccount(DTOLibrary.AccountLogin acc)
        {
            DTOLibrary.THONGTIN accInfos = daoAccount.getAccountInfos(acc.Email, acc.Password);
            if (accInfos == null)
                return false;
            return true;
        }
        public List<DTOLibrary.TaiNguyen> getListResource(String account, DateTime from, int nums)
        {
            return daoAccount.getListResource(account, from, nums);
        }

        //public void changeAvatarPath(

        public int getNextFileId(String type)
        {
            return daoAccount.getMaxFileId(type) + 1;
        }

        public void changeAvatar(String email, int id)
        {
            daoAccount.changeAvatar(email, id);
        }

        public void changeCover(String email, int id)
        {
            daoAccount.changeCover(email, id);
        }

        public void addNewUser(DTOLibrary.AccountRegister acc)
        {
            
            daoAccount.addNewUser(acc);

            Album alb = new Album();
            //addNewUserInfo(acc.Email, acc.Name);
            alb.TieuDe = "Album tạm";
            alb.NoiDung = "Album này chứa những hình ảnh tạm thời";
            daoAccount.addAlbum(acc.Email, alb); 
            daoAccount.changeAvatar(acc.Email, 1);
            daoAccount.changeCover(acc.Email, 2);

        }

        //public static  void addNewUserInfo(String email,String name)
        //{
        //    DAOLibrary.Account.addNewUserInfo(email,name);
        //}
        public void addImage(String email, HinhAnh hinh, int maalbum)
        {
            daoAccount.addImage(email, hinh, maalbum);
        }

        public int getIdAlbum(String email, String nameAlbum)
        {
            return daoAccount.getIdAlbum(email, nameAlbum);
        }
        public void addAlbum(String email, Album alb)
        {
            daoAccount.addAlbum(email, alb);
        }
		
		
    }
}

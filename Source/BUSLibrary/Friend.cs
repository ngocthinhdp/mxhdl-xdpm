﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BUSLibrary
{
    public class Friend
    {
        public static String getFriendState(String email1, String email2)
        {
            return DAOLibrary.Friend.getFriendState(email1, email2);
        }

        public static void sendRequest(String email1, String email2)
        {
            DAOLibrary.Friend.sendRequest(email1, email2);
        }

        public static void confirmRequest(String email1, String email2)
        {
            DAOLibrary.Friend.confirmRequest(email1, email2);
        }

        public static List<DTOLibrary.FriendInfo> getListFriend(String email, String state)
        {
            return DAOLibrary.Friend.getListFriend(email, state);

        }
    }
}

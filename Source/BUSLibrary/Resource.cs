﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAOLibrary;
using DTOLibrary;


namespace BUSLibrary
{
    public class RetrievedPost
    {
        DAOLibrary.Resource busRes = new DAOLibrary.Resource();
        DAOLibrary.Account busAcc = new DAOLibrary.Account();
        public List<DTOLibrary.RetrievedPost> getUserPosts(String keyword)
        {
            List<DTOLibrary.RetrievedPost> result = busRes.getUserPosts(keyword);
            return result;
        }

        public DTOLibrary.RetrievedPost getUserPostById(String id)
        {
            return busRes.getUserPostById(id);
        }
        public List<DTOLibrary.RetrievedPost> getSystemPosts(String keyword)
        {
            List<DTOLibrary.RetrievedPost> result = busRes.getSystemPosts(keyword);
            return result;
        }

        public DTOLibrary.RetrievedPost getSystemPostById(String id)
        {
            return busRes.getSystemPostById(id);
        }

        public DTOLibrary.BaiViet getPost(int id)
        {
            return busAcc.getPost(id);
        }
    }
    public class SubmittedPost
    {
        DAOLibrary.Resource busRes = new DAOLibrary.Resource();
        public bool submitUserPost(DTOLibrary.SubmittedPost post)
        {
            return busRes.submitUserPost(post);
        }
        public bool submitUserPost(String email,DTOLibrary.SubmittedPost post)
        {
            return busRes.submitUserPost(email, post);
        }
    }

    public class RatingInfo
    {
        DAOLibrary.Resource busRes = new DAOLibrary.Resource();
        public bool ratePlace(DTOLibrary.RatingInfo info)
        {
            return busRes.ratePlace(info);
        }
        public double getAvgScore(String placeId)
        {
            return busRes.getAvgScore(placeId);
        }
    }
}
